package com.example.sgcmusuarios.service;

import java.util.*;

import com.example.sgcmusuarios.dto.*;

public interface IUserService {
	public Iterable<User> getUsers();
	public Optional<User> findById(Long id);
	public User save(User user);
	public void deleteById (Long id);
	
	public Iterable<User> findAllById(Iterable<Long> ids);
}


/*public Iterable<Alumno> findAll();
public Page<Alumno> findAll(Pageable pageable);
public Optional<Alumno> findById(Long id);
public Alumno save(Alumno alumno);
public void deleteById(Long id);
public List<Alumno> findByNombreOrApellido(String term);

//comunicación
public Iterable<Alumno> findAllById(Iterable<Long> ids);

public void eliminarCursoAlumnoPorId(Long id);*/