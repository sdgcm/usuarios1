package com.example.sgcmusuarios.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

import com.example.sgcmusuarios.dao.*;
import com.example.sgcmusuarios.dto.*;

@Service
public class UserService implements IUserService{

	@Autowired
	private UserRepository repository;
	
	@Override
	@Transactional(readOnly = true)
	public Iterable getUsers() {
		
		return repository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Optional findById(Long id) {
		
		return repository.findById(id);
	}

	@Override
	@Transactional
	public User save(User user) {
		
		return repository.save(user);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		repository.deleteById(id);
		
	}

	@Override
	@Transactional(readOnly = true)
	public Iterable<User> findAllById(Iterable<Long> ids) {
		
		return repository.findAllById(ids);
	}



}
